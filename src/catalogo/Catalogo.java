package src.catalogos;
import java.io.*;
import java.util.*;

public class Catalogo{

	private ArrayList<Coche> catalogo = new ArrayList<>();	
	private String nombreFichero = "coches.txt";
	
	
	public Catalogo() {
		cargarCoches();
	}

	public void add(Coche v){
		catalogo.add(v);
	}
	
	public void annadir(Coches v){
		catalogo.add(v);
		volcarCoches();
 		System.out.println("Coche modificado o/y includio.");
	}
	
	// Este comando es para borrar objetos del ArrayList. 
	public void borrar(Coche v){
		catalogo.remove(v);
		volcarCoches();
		System.out.println("Coche borrado.");
	}
	
	// Aqui se lee el fichero en donde se guardan los objetos y este convierte las líneas char en objetos que son guardadas en el Array.
	private void cargarCoches(){
			try{
				File fichero = new File(nombreFichero);
				fichero.createNewFile(); 
				Scanner sc = new Scanner(fichero);
				sc.useDelimiter(",|\n");
				while(sc.hasNext()){
					catalogo.add(new Coche (sc.next(), sc.next(), sc.nextInt()));
				}
		}catch(IOException ex){
			System.out.println("El catalogo esta vacio");
		}
	}
	
	// Este comando nos deja escribir en un fichero .txt los objetos del array esto esta asi para que una ves se cierre el programa este siga guardado. 
	private void volcarCoches(){
		try{
			FileWriter fw = new FileWriter(nombreFichero);
			for(Coche v : catalogo){
				fw.write(v.getMarca() + "\n");
				fw.write(v.getTipo() + "\n");
				fw.write(v.getPrecio() + "\n");

			}
			
			fw.close();
		}catch(IOException ex){
			System.err.print(ex);
		}
	}
	
	// Aqui estamos generando una hoja de calculo que guarda los Coches y sus atributos 
	public void generarHojaCalculo(){
		try{
			FileWriter fw = new FileWriter(nombreFichero);
			fw.write("Marca,Tipo,Precio\n");
			fw.write(toString().trim());
			fw.close();
                  }catch(IOException ex){
			  System.err.println(ex);
		  }
	}


	public String toString(){
		StringBuilder sb = new StringBuilder();
		for(Coche v  : catalogo)
		sb.append(v + "\n");

	return sb.toString();
	}	
}


