package src.catalogos;

public class Coches{

	private String marca;
	private String tipo;
	private int precio; 

	// Aqui se esta creando un Constructor sin parámetros
	public Coches(){}

	// Esto es un constructor con un solo parámetro para que se pueda usar como borrado del Array List
	public Coches(String marca){
	this.marca = marca;
	}

	 public Coches(String marca, String tipo){
                this.marca = marca;
                this.tipo = tipo;
	}

	// Este es un constructor con parámetros de los objetos de la clase Coches 
	public Coches(String marca, String tipo, int precio) {
		this.marca = marca;
		this.tipo = tipo;
		this.precio = precio;
	}

	// Aqui tenemos un comando que le dice al programa que todos los objetos con el mismo atributo son el mismo objeto a la hora de compararlos
	public boolean equals(Object coches){
		Coches v = (Coches) coches;
		return this.marca.equals(v.marca);
	}

	
	public String toString(){
		return  marca + ", " + tipo + ", " + precio;
	}

	public String getMarca(){
		return marca;
	}

	public String getTipo(){
		return tipo;
	}

	public int getPrecio(){
		return precio;
	}
}

